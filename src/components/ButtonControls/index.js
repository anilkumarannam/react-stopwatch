import { Component } from "react";

class ButtonControls extends Component {

  state = { buttonOneText: "Start" };

  handleClick = () => {
    const { actions } = this.props;
    if (this.state.buttonOneText === "Start") {
      (actions.start)();
      this.setState({ buttonOneText: "Stop" });
    } else if (this.state.buttonOneText === "Stop") {
      (actions.stop)();
      this.setState({ buttonOneText: "Resume" });
    } else if (this.state.buttonOneText === "Resume") {
      (actions.resume)();
      this.setState({ buttonOneText: "Stop" });
    }
  }

  reset = () => {
    this.setState({ buttonOneText: "Start" });
    (this.props.actions.reset)();
  }

  render = () => {
    const buttonControls = (
      <div>
        <button onClick={this.handleClick}>{this.state.buttonOneText}</button>
        <button onClick={this.reset}>Reset</button>
      </div>
    );
    return buttonControls;
  }

}

export default ButtonControls;