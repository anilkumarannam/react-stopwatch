import { Component } from "react";

class SpanComponent extends Component {

  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }

  render = () => {
    const { limit, threshold, time } = this.props;
    const timeUnits = Math.floor(time / threshold) % limit;
    return <span>{timeUnits < 10 ? "0" + timeUnits : timeUnits}</span>
  }
}

export default SpanComponent;