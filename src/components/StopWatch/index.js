import SpanComponent from '../SpanComponent';
import ButtonControls from '../ButtonControls';

const { Component } = require("react");

class StopWatch extends Component {
  constructor(props) {
    super(props);
    this.state = { time: 0 };
    this.intervalId = null;
  }


  start = () => {
    this.intervalId = setInterval(() => {
      this.setState({ time: this.state.time + 1 });
    }, 10);
  }

  stop = () => {
    clearInterval(this.intervalId);
  }

  resume = () => {
    this.intervalId = setInterval(() => {
      this.setState({ time: this.state.time + 1 });
    }, 10);
  }

  reset = () => {
    this.stop();
    this.setState({ time: 0 });
  }

  render() {
    const { time } = this.state;
    const container = (
      <div>
        <h1>
          <SpanComponent limit={24} threshold={60 * 60 * 100} time={time} ></SpanComponent> :
          <SpanComponent limit={60} threshold={60 * 100} time={time}></SpanComponent> :
          <SpanComponent limit={60} threshold={100} time={time} ></SpanComponent> :
          <SpanComponent limit={100} threshold={1} time={time} ></SpanComponent>
        </h1>
        <ButtonControls actions={{ start: this.start, stop: this.stop, resume: this.resume, reset: this.reset }}></ButtonControls>
      </div >
    );
    return container;
  }

}

export default StopWatch;