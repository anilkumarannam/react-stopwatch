import StopWatch from './components/StopWatch';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <StopWatch></StopWatch>
      </header>
    </div>
  );
}

export default App;
